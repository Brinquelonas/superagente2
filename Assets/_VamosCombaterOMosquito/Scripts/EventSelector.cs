﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventSelector : MonoBehaviour
{
    [System.Serializable]
    public class SelectorEvent : UnityEvent { }

    public SelectorEvent OnClick = new SelectorEvent();

    public void OnMouseUpAsButton()
    {
        OnClick.Invoke();
    }

}
