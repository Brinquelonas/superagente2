﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Spine.Unity;
using TMPro;

public class QuestionInteraction_NonAR : MonoBehaviour {

    [System.Serializable]
    public struct InteractionConfig
    {
        public string Tag;
        public Interaction Interaction;
    }

	public Image Balloon;
	public TextMeshProUGUI Question;
	public Button[] Buttons;
	public Button[] ImageButtons;
    public List<NPCController> NPCs = new List<NPCController>();
    public List<InteractionConfig> Interactions = new List<InteractionConfig>();
    public string ResourcesPath = "";

	[Header("Score")]
	public TextMeshProUGUI ScoreText;

    [Header("Player")]
    public QuizPlayer PlayerPrefab;
    public List<QuizPlayer> Players;

    private int _playerIndex;

    private QuizPlayer _currentPlayer
    {
        get
        {
            return Players[_playerIndex];
        }
    }

    [Header("UI")]
    public ReportScroll ReportScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;

	private int _score;
	public int Score
	{
		get
		{
			return _score;
		}
		set 
		{
			_score = value;
			//ScoreText.text = _score.ToString();
		}
	}

    private string _log;

	//private bool _found = false;
	private Question _currentQuestion;
	private List<Question> _questions = new List<Question> ();
	private int _questionIndex;
    public int QuestionIndex
    {
        get
        {
            return _questionIndex;
        }
        set
        {
            _questionIndex = value;
            ScoreText.text = _questionIndex.ToString() + "/5";
        }
    }

    public int Space { get; set; }
    public int CorrectAnswer { get; set; }

    public NPCController CurrentNPC
    {
        get
        {
            return NPCs[_currentQuestion.NPCIndex - 1];
        }
    }

	protected void Awake()
	{
		for (int i = 0; i < Buttons.Length; i++) 
		{
			InitializeButton(i);
		}
        for (int i = 0; i < ImageButtons.Length; i++)
        {
            InitializeImageButton(i);
        }

        if (GameConfigs.Players == null || GameConfigs.Players.Count == 0)
            GameConfigs.Players = new List<PlayerConfig>() { new PlayerConfig("teste", "1", Color.red), new PlayerConfig("teste2", "2", Color.blue) };

        for (int i = 0; i < GameConfigs.Players.Count; i++)
        {
            QuizPlayer player = Instantiate(PlayerPrefab, transform);
            player.Initialize(GameConfigs.Players[i].Name, GameConfigs.Players[i].Number, GameConfigs.Players[i].Color);
            player.SetActive(i == 0);

            player.Log += player.Name + "\n";

            Players.Add(player);
        }

        _log += "Modo Quiz\n\n";
    }

	void Start()
	{
		_questions = QuestionsTSVReader.Instance.GetTXTQuestions();

		OnHandlerFound (0);
	}    

	void InitializeButton(int index)
	{
		Buttons[index].onClick.AddListener(() => Answer (index));
	}

    void InitializeImageButton(int index)
    {
        ImageButtons[index].onClick.AddListener(() => Answer(index));
    }

    void EnableButtons()
	{
        for (int i = 0; i < Buttons.Length; i++)
            Buttons[i].enabled = true;
        for (int i = 0; i < ImageButtons.Length; i++)
            ImageButtons[i].enabled = true;
    }

    void DisableButtons()
	{
		for (int i = 0; i < Buttons.Length; i++) 
			Buttons [i].enabled = false;
        for (int i = 0; i < ImageButtons.Length; i++)
            ImageButtons[i].enabled = false;
    }

    public void OnHandlerFound(int space)
	{
		EnableButtons ();

        Space = space;
        CorrectAnswer = Random.Range(0, 3);

        if (_playerIndex == 0)
		    QuestionIndex++;		
		//if (QuestionIndex > 1) {
        if (QuestionIndex > 5) {
			EndGame ();
			return;
		}

		int questionIndex = Random.Range(0, _questions.Count);

		_currentQuestion = _questions[questionIndex];
		_questions.Remove(_currentQuestion);

        Question.text = _currentQuestion.Text;
        Balloon.gameObject.SetActive(false);

        _currentPlayer.Log += "\n" + _currentQuestion.Text + "\n";

        CurrentNPC.gameObject.SetActive(true);
        PlaySpineAnimation ("waiting", true);


        switch (_currentQuestion.Type)
        {
            case QuestionType.None:
                break;
            case QuestionType.TXT:
                InitializeTXTQuestion();
                break;
            case QuestionType.IMG:
                InitializeIMGQuestion();
                break;
            case QuestionType.INT:
                InitializeINTQuestion();
                break;
            default:
                break;
        }

        PotaTween.Create(CurrentNPC.gameObject).Stop();
        PotaTween.Create(CurrentNPC.gameObject).
            SetAlpha(0f, 1f).
            SetDuration(0.5f).
            Play(() => 
            {
                PlaySpineAnimation("question", false , () =>
                {
                    PlaySpineAnimation("waiting", true);
                });
                ShowBalloon(() =>
                {
                    PlayAudio(_currentQuestion.Audio, () => 
                    {
                        StartCoroutine(ShowAnswers());
                    });
                });
            });
	}

    public void OnHandlerFoundInteraction(int space)
    {
        
    }

    public void Answer(int answer)
	{
		DisableButtons();

		System.Action callback = () => 
		{
            HideAll();
		};

        _currentPlayer.Log += "Resposta certa: " + _currentQuestion.RightAnswer + "\n";
        _currentPlayer.Log += "Resposta dada: " + Buttons[answer].GetComponentInChildren<TextMeshProUGUI>().text + "\n";

        if (answer == CorrectAnswer)
        {
            PlaySpineAnimation("right", false, callback);
            PlayAudio(null);
            SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Positive);
			//Question.text = "Parabéns!\n" + result.Text;
            Question.text = "Parabéns!";

            _currentPlayer.RightAnswers++;
            _currentPlayer.Log += "Correto!\n";

            Score++;
        }
		else
        {
			PlaySpineAnimation("wrong", false, callback);
            PlayAudio(null);
            SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Negative);
			//Question.text = "Que pena...\n" + result.Text;
            Question.text = "Que pena...";

            _currentPlayer.WrongAnswers++;
            _currentPlayer.Log += "Errado...\n";
        }

        StartCoroutine(HideWrongAnswers());
	}

    private void NextPlayer()
    {
        _playerIndex++;
        _playerIndex %= Players.Count;

        for (int i = 0; i < Players.Count; i++)
        {
            Players[i].SetActive(i == _playerIndex);
        }

        OnHandlerFound(0);
    }

    public void EndGame()
	{
		Balloon.gameObject.SetActive(false);

		CurrentNPC.gameObject.SetActive(true);
		PlaySpineAnimation("right", false);

		PotaTween.Create(CurrentNPC.gameObject).Stop();
		PotaTween.Create(CurrentNPC.gameObject).
		SetAlpha(0f, 1f).
		SetDuration(0.5f).
		Play(() =>
		{
			Question.text = "";
			ShowBalloon(() =>
			{
                List<QuizPlayer> players = Players.OrderByDescending((p) => p.RightAnswers).ToList();

                //PlaySpineAnimation("right", false);
                PlayAudio(null);
				SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Positive);
				//Question.text = "Parabéns!\n" + result.Text;
				Question.text = "O ganhador é: " + players[0].Name + "! Parabéns!\nVocê fez " + players[0].RightAnswers + " pontos!";

				InitializeEndGame();
			});
		});
	}

	private void InitializeEndGame()
	{
        HideElement(ScoreText.gameObject);

        Button btn = Instantiate(Buttons[1], transform);
		btn.gameObject.SetActive (true);
		btn.GetComponentInChildren<TextMeshProUGUI>().text = "Relatório";
        //btn.GetComponentInChildren<TextMeshProUGUI>().text = "Voltar à tela inicial";
		btn.onClick.RemoveAllListeners ();
		btn.onClick.AddListener (() => {
            //SceneManager.LoadScene(0);

            for (int i = 0; i < Players.Count; i++)
            {
                Players[i].Log += "\nTotal de respostas certas: " + Players[i].RightAnswers;
                Players[i].Log += "\nTotal de respostas erradas: " + Players[i].WrongAnswers;
                Players[i].Log += "\n-------------------------------------------------------\n";

                _log += Players[i].Log;
            }

            ReportScroll.SetActive(true);
            ReportScroll.Text.text = _log;

            HideElement(CurrentNPC.gameObject);
            HideElement(Balloon.gameObject);
            HideElement(btn.gameObject);
        });
		PotaTween.Create (btn.gameObject).SetAlpha (0f, 1f).SetDuration (0.1f).Play ();
	}

    public void ScrollSendMailButtonClicked()
    {
        ReportScroll.SetActive(false);
        SendMailPopup.SetActive(true);
    }

    public void SendMailPopupButtonClicked()
    {
        SendMailPopup.SendMailButtonClicked(_log, () => 
        {
            SendMailPopup.SetActive(false);
            AlertPopup.SetActive(true, 1);
            AlertPopup.Text.text = "Relatório enviado com sucesso! Enviar relatório para outro e-mail?";
            AlertPopup.Buttons[0].onClick.AddListener(() => { SceneManager.LoadScene("Start"); });
            AlertPopup.Buttons[1].onClick.AddListener(() => 
            {
                AlertPopup.SetActive(false);
                SendMailPopup.SetActive(true);
            });
        }, () => 
        {
            SendMailPopup.SetActive(false);
            AlertPopup.SetActive(true);
            AlertPopup.Text.text = "Falha ao enviar o relatório. Verifique sua conexão com a internet e tente novamente.";
            AlertPopup.Buttons[0].onClick.AddListener(() => 
            {
                AlertPopup.SetActive(false);
                SendMailPopup.SetActive(true);
            });
        });
    }

    private void InitializeTXTQuestion()
    {
        List<string> wrongAnswers = new List<string>(_currentQuestion.WrongAnswers);
        for (int i = 0; i < Buttons.Length; i++)
        {
            if (i == CorrectAnswer)
            {
                Buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = _currentQuestion.RightAnswer;
            }
            else
            {
                int answerIndex = Random.Range(0, wrongAnswers.Count);
                Buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = wrongAnswers[answerIndex];
                wrongAnswers.RemoveAt(answerIndex);
            }
            Buttons[i].gameObject.SetActive(false);
        }
    }

    private void InitializeIMGQuestion()
    {
        List<string> wrongAnswers = new List<string>(_currentQuestion.WrongAnswers);
        for (int i = 0; i < ImageButtons.Length; i++)
        {
            string txt = "";
            if (i == CorrectAnswer)
            {
                txt = _currentQuestion.RightAnswer;
            }
            else
            {
                int answerIndex = Random.Range(0, wrongAnswers.Count);
                txt = wrongAnswers[answerIndex];
                wrongAnswers.RemoveAt(answerIndex);
            }
            Image img = ImageButtons[i].transform.Find("Image").GetComponent<Image>();
            img.sprite = Resources.Load<Sprite>(ResourcesPath + "/" + txt);
            img.SetNativeSize();
            ImageButtons[i].gameObject.SetActive(false);
        }
    }

    private void InitializeINTQuestion()
    {
        for (int i = 0; i < Interactions.Count; i++)
        {
            if (Interactions[i].Tag == _currentQuestion.RightAnswer)
            {
                Interactions[i].Interaction.gameObject.SetActive(true);
                Interactions[i].Interaction.StartInteraction(Space);
                Interactions[i].Interaction.OnFinish.AddListener((b) =>
                {
                    if (b)
                        Answer(CorrectAnswer);
                    else
                        Answer(-1);
                    Interactions[i].Interaction.OnFinish.RemoveAllListeners();
                });

                PotaTween.Create(Interactions[i].Interaction.gameObject).
                    SetAlpha(0f, 1f).
                    SetDuration(0.5f).
                    Play();

                break;
            }
        }
    }

    private void PlayAudio(AudioClip clip, System.Action callback = null)
    {
        if (clip == null)
        {
            if (callback != null) callback();
            CurrentNPC.PlayMouthAnimation(2, () => CurrentNPC.PlayMouthAnimation(CurrentNPC.CurrentAnimationState));
            return;
        }

        AudioSource.PlayClipAtPoint(clip, Vector3.zero);

        if (callback != null)
            StartCoroutine(AudioCallback(clip, callback));
    }

    private void PlaySpineAnimation(string animationName, bool loop, System.Action callback = null)
    {
        CurrentNPC.PlayAnimation(animationName, loop, callback);
    }

    private void ShowBalloon(System.Action callback = null)
    {
        Balloon.gameObject.SetActive(true);

        PotaTween.Create(Balloon.gameObject).Stop();
        PotaTween.Create(Balloon.gameObject).
            SetAlpha(0f, 1f).
            SetScale( new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play(callback);
    }

    private void HideButton(int i)
    {
        PotaTween.Create(Balloon.gameObject).Stop();
        if (_currentQuestion.Type == QuestionType.TXT)
        {
            PotaTween.Create(Buttons[i].gameObject).
                SetAlpha(1f, 0f).
                SetScale(Vector3.one, new Vector3(0.8f, 0.8f, 1)).
                SetDuration(0.5f).
                SetEaseEquation(Ease.Equation.OutSine).
                Play(() => 
                {
                    Buttons[i].gameObject.SetActive(false);
                });
        }
        else if (_currentQuestion.Type == QuestionType.IMG)
        {
            PotaTween.Create(ImageButtons[i].gameObject).
                SetAlpha(1f, 0f).
                SetScale(Vector3.one, new Vector3(0.8f, 0.8f, 1)).
                SetDuration(0.5f).
                SetEaseEquation(Ease.Equation.OutSine).
                Play(() =>
                {
                    Buttons[i].gameObject.SetActive(false);
                });
        }
    }

    private void HideAll()
    {
        HideElement(Balloon.gameObject);
        if (_currentQuestion.Type == QuestionType.TXT)
        {
            HideElement(Buttons[CorrectAnswer].gameObject, () =>
            {
            });
        }
        else if (_currentQuestion.Type == QuestionType.IMG)
        {
            HideElement(ImageButtons[CorrectAnswer].gameObject, () =>
            {
            });
        }
        else
        {
            for (int i = 0; i < Interactions.Count; i++)
            {
                if (Interactions[i].Interaction.gameObject.activeSelf)
                {
                    Interaction interaction = Interactions[i].Interaction;
                    DisableInteraction(interaction);
                }
            }
        }
        HideElement(CurrentNPC.gameObject, () => 
        {
            CurrentNPC.gameObject.SetActive(false);
            //GetComponent<Canvas>().enabled = false;
            //_found = false;

			//gameObject.SetActive(false);

			//OnHandlerFound(0);

			Invoke("Found", 0.5f);
        });
    }

	private void Found()
	{
        //OnHandlerFound(0);

        NextPlayer();
	}

    private void DisableInteraction(Interaction interaction)
    {
        HideElement(interaction.gameObject, () =>
        {
            interaction.gameObject.SetActive(false);
        });
    }

    private void HideElement(GameObject element, System.Action callback = null)
    {
        PotaTween.Create(element, 1).Stop();
        PotaTween.Create(element, 1).
            SetAlpha(1f, 0f).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() =>
            {
                if (callback != null)
                    callback();

                element.SetActive(false);
            });
    }

    private IEnumerator ShowAnswers()
    {
        Button[] buttons;
        if (_currentQuestion.Type == QuestionType.TXT)
            buttons = Buttons;
        else if (_currentQuestion.Type == QuestionType.IMG)
            buttons = ImageButtons;
        else
            buttons = new Button[0];

        DisableButtons();

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].gameObject.SetActive(true);

            PotaTween.Create(buttons[i].gameObject).Stop();
            PotaTween.Create(buttons[i].gameObject).
            SetAlpha(0f, 1f).
            SetScale(new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play();

            yield return new WaitForSeconds(0.2f);
        }

        EnableButtons();
    }

    private IEnumerator HideWrongAnswers()
    {
        Button[] buttons;
        if (_currentQuestion.Type == QuestionType.TXT)
            buttons = Buttons;
        else if (_currentQuestion.Type == QuestionType.IMG)
            buttons = ImageButtons;
        else
            buttons = new Button[0];

        for (int i = 0; i < buttons.Length; i++)
        {
            if (i != CorrectAnswer)
            {
                HideButton(i);

                yield return new WaitForSeconds(0.2f);
            }

        }
    }

    private IEnumerator AudioCallback(AudioClip clip, System.Action callback)
    {
        float elapsedTime = 0;

        while(elapsedTime < clip.length)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        callback();
    }
}
