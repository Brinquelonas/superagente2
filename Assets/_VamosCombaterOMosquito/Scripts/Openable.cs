﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Openable : MonoBehaviour {

    public bool IsOpen
    {
        get
        {
            return gameObject.activeSelf;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            if (_tween == null)
                _tween = PotaTween.Create(gameObject).
                    SetScale(Vector3.one * 0.5f, Vector3.one).
                    SetAlpha(0f, 1f).
                    SetDuration(0.3f);

            return _tween;
        }
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Stop();
            Tween.Play();
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);
            });
        }
    }
}
