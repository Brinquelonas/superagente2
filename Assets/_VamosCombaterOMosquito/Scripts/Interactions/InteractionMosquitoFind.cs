﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractionMosquitoFind : Interaction {

    public ModelInteraction MosquitoPrefab;
    public Transform EventsParent;
    public QuestionInteraction QuestionCanvas;
    public CanvasTrackableEventHandler MosquitoModelInteraction;
    public TextMeshProUGUI CounterTextMesh;
    public TextMeshProUGUI MessageTextMesh;
    public TextMeshProUGUI WinMessageTextMesh;
    public AudioClip WinMessageAudio;

    private float _gameTime = 20;
    private ModelInteraction _mosquito;
    private List<EventSelector> _spaces;
    private GameObject _mosquitoPivot;
    private bool _stopCounter;

    private List<CanvasTrackableEventHandler> Targets
    {
        get
        {
            return new List<CanvasTrackableEventHandler>(GetComponentsInChildren<CanvasTrackableEventHandler>(true));
        }
    }

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.spatialBlend = 0;
            _audioSource.playOnAwake = false;

            return _audioSource;
        }
    }

    private void Start()
    {
        StartWaitCounter();
    }

    private void StartGame()
    {
        _spaces = new List<EventSelector>(EventsParent.GetComponentsInChildren<EventSelector>(true));

        int minIndex = 0;
        int maxIndex = Targets.Count;

        int targetIndex = Random.Range(minIndex, maxIndex);

        bool targetEnabled = true;
        if (!Targets[targetIndex].gameObject.activeSelf)
        {
            targetEnabled = false;
            Targets[targetIndex].gameObject.SetActive(true);
        }

        _mosquito = Instantiate(MosquitoPrefab);
        _mosquito.OnClickEvent.AddListener(OnMosquitoClick);

        _mosquito.GetComponent<Animator>().SetBool("Animate", true);

        _mosquitoPivot = new GameObject("Pivot");
        _mosquitoPivot.transform.SetParent(Targets[targetIndex].transform);
        _mosquitoPivot.transform.localPosition = Vector3.zero;
        _mosquito.transform.SetParent(_mosquitoPivot.transform);
        _mosquito.transform.localPosition = new Vector3(30, 10, 0);

        PotaTween tween = PotaTween.Create(_mosquitoPivot).
                SetRotation(Vector3.zero, Vector3.up * 360).
                SetDuration(3).
                SetLoop(LoopType.Loop);
        tween.PlayOnEnable = true;
        tween.Play();

        EnableSpaces(false);

        StartCoroutine(StartCounter(_gameTime, true, () =>
        {
            OnTimeOver();
        }));

        if (!targetEnabled)
            Targets[targetIndex].gameObject.SetActive(false);
    }

    private void OnMosquitoClick(ModelInteraction mosquito)
    {
        Destroy(_mosquitoPivot.gameObject);
        WinAnimation();
    }

    private void OnTimeOver()
    {
        Destroy(_mosquitoPivot.gameObject);
        EnableSpaces(true);

        OnFinish.Invoke(false);

        StartWaitCounter();
    }

    private void EnableSpaces(bool enable)
    {
        for (int i = 0; i < _spaces.Count; i++)
            _spaces[i].gameObject.SetActive(enable);
        MosquitoModelInteraction.gameObject.SetActive(enable);
    }

    private void StartWaitCounter()
    {
        float time = Random.Range(60, 120);
        StartCoroutine(StartCounter(time, false, StartGame));
    }

    private void WinAnimation()
    {
        _stopCounter = true;
        WinMessageTextMesh.gameObject.SetActive(true);

        PlayAudio(WinMessageAudio);

        PotaTween.Create(WinMessageTextMesh.gameObject).
            SetScale(Vector3.zero, Vector3.one).
            SetAlpha(0f, 1f).
            SetDuration(0.5f).
            Play();

        StartCoroutine(StartCounter(3f, false, () =>
        {
            EnableSpaces(true);

            OnFinish.Invoke(true);

            WinMessageTextMesh.gameObject.SetActive(false);
            StartWaitCounter();
        }));
    }

    private void PlayAudio(AudioClip clip)
    {        
        AudioSource.PlayOneShot(clip);
    }

    private IEnumerator StartCounter(float time, bool display, System.Action callback)
    {
        _stopCounter = false;

        CounterTextMesh.gameObject.SetActive(display);
        MessageTextMesh.gameObject.SetActive(display);

        float elapsedTime = 0;
        while (elapsedTime < time)
        {
            if(_stopCounter)
                yield break;

            if (!QuestionCanvas.gameObject.activeSelf)
                elapsedTime += Time.deltaTime;

            if (CounterTextMesh.gameObject.activeSelf)
                CounterTextMesh.text = ((int)(time - elapsedTime)).ToString();

            yield return null;
        }

        if (callback != null)
            callback();
    }
}
