﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Spine.Unity
{
    /// <summary>Sets a GameObject's transform to match a bone on a Spine skeleton.</summary>
    [ExecuteInEditMode]
    [AddComponentMenu("Spine/UIBoneFollower")]
    public class SpineUIBoneFollower : MonoBehaviour
    {

        #region Inspector
        public SkeletonGraphic skeletonRenderer;
        public SkeletonGraphic SkeletonRenderer
        {
            get { return skeletonRenderer; }
            set
            {
                skeletonRenderer = value;
                Initialize();
            }
        }

        /// <summary>If a bone isn't set in code, boneName is used to find the bone.</summary>
        [SpineBone(dataField: "skeletonRenderer")]
        public String boneName;

        public bool followZPosition = true;
        public bool followBoneRotation = true;
        public float rotationTolerance = 0.0f;

        [Tooltip("Follows the skeleton's flip state by controlling this Transform's local scale.")]
        public bool followSkeletonFlip = true;

        [Tooltip("Follows the target bone's local scale. BoneFollower cannot inherit world/skewed scale because of UnityEngine.Transform property limitations.")]
        public bool followLocalScale = false;

        [UnityEngine.Serialization.FormerlySerializedAs("resetOnAwake")]
        public bool initializeOnAwake = true;
        #endregion

        [NonSerialized] public bool valid;
        [NonSerialized] public Bone bone;
        RectTransform skeletonTransform;

        public void Awake()
        {
            if (initializeOnAwake) Initialize();
        }

        public void HandleRebuildRenderer(SkeletonGraphic skeletonRenderer)
        {
            Initialize();
        }

        public void Initialize()
        {
            bone = null;
            valid = skeletonRenderer != null && skeletonRenderer.IsValid;
            if (!valid) return;

            skeletonTransform = skeletonRenderer.rectTransform;
            skeletonRenderer.OnRebuild += HandleRebuildRenderer;
            skeletonRenderer.OnRebuild -= HandleRebuildRenderer;

            if (!string.IsNullOrEmpty(boneName))
                bone = skeletonRenderer.Skeleton.FindBone(boneName);

#if UNITY_EDITOR
            if (Application.isEditor)
                LateUpdate();
#endif
        }

        void OnDestroy()
        {
            if (skeletonRenderer != null)
                skeletonRenderer.OnRebuild -= HandleRebuildRenderer;
        }

        public void LateUpdate()
        {
            if (!valid)
            {
                Initialize();
                return;
            }

            if (bone == null)
            {
                if (string.IsNullOrEmpty(boneName)) return;
                bone = skeletonRenderer.Skeleton.FindBone(boneName);
                if (bone == null)
                {
                    Debug.LogError("Bone not found: " + boneName, this);
                    return;
                }
            }

            RectTransform thisTransform = this.GetComponent<RectTransform>();
            thisTransform.position = skeletonTransform.TransformPoint(new Vector3(bone.worldX, bone.worldY, 0) * 100);
            if (followBoneRotation) thisTransform.rotation = Quaternion.Euler(0f, 0f, bone.WorldRotationX + rotationTolerance);

            Vector3 localScale = followLocalScale ? new Vector3(bone.scaleX, bone.scaleY, 1f) : Vector3.one;
            if (followSkeletonFlip) localScale.y *= bone.skeleton.flipX ^ bone.skeleton.flipY ? -1f : 1f;
            thisTransform.localScale = localScale;
        }
    }

}
